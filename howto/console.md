# Setting up a Console

Now that your Rpi with Chariot is working, it is time to move on to something more useful. How about adding a console using which you interact with the system. While I was tempted to develop a console that uses the HDMI and USB keyboard & mouse capabilities of the RPi, it is really inappropriate to hook up all of these to a device that should be have a minimum footprint. So, IMO, the best option is to hook up a small TFT display and a few push buttons to the RPi to build a console that works with the Chariot. While you can plugin to Chariot using your own console hardware and driver, I have created one and want to share with you how mine works and you will have the additional benefit of using the software that I have already created.

## Building the hardware:

To build a console, you will need the following:

1. Adafruit ILI9340 TFT 3.3" 240x320 pixel
2. Two push buttons - I bought mine from Amazon. The make is Microtivity IM206
3. Two 10 KOHM Resistors
4. One Trim Pot - I bought mine from Amazon The make is Adafruit 356 Trim Potentiometer.
5. You also need wires. Single strand wires are better.
6. You will need a prototype PCB where you can create your own circuit.
7. You will need some solder and a soldering iron.
8. If you want to prototype before building it, you will also need a bread board.

Item 4, above, is not needed if you don't want to control the brightness of your LCD screen.

Once you got all the items, you need to wire them up as shown in the schematics - https://bitbucket.org/iot4all/chariot/src/schematics/Chariot-Console.png

## Prerequisite Software

You will need the following python modules to be installed.

1. https://github.com/adafruit/Adafruit_Python_ILI9341 - Follow the instructions for installing the module. Instead of the **pip**, use **pip3** and **python3** instead of **python** command since we use python3.
2. https://github.com/adafruit/Adafruit_Python_GPIO - Follow the instructions for installing the module. Instead of the **pip**, use **pip3** and **python3** instead of **python** command since we use python3.

## Configuring Chariot to use the Console

Once the hardware is built and wired, edit the Chariot properties file that you created earlier and add the following lines:

> **displayDriver=adafruit_ili9340**

> **userInputDriver=gpio_2pushbutton**

Save the configuration and restart the Chariot application. If the wiring is correct, you will see a menu displayed on the screen and you will be able to browse through the menu using the **NEXT** button (one of the push buttons). You can also click on the **ENTER** button (the other push button) to select a menu and go to the next level.

## Creating Your Own Console

If you already have a TFT module from another vendor, you can create your own display driver and plugin into the Chariot framework. To do this, the following steps are required:

1. Create a python file and name it driver_display_**something**.py in the **drivers** directory. Change **something** to something meaningful.
2. Add the following functions and code it using the python modules provided by the hardware vendor:
  * def init()
  * def clear(color)
  * draw_text(text, row, col, reverse=False)
  * Note - I will add more functions in the future as I enhance the functionality of the console.
3. Edit the properties file in the **/etc** directory that you created earlier and add the driver information by adding the following line:
  ..* displayDriver=**something**
4. Restart Chariot. If everything works, you should see the menu or some printed information.

Similarly, if you want to your own input device circuit instead of the two push buttons provided by the circuit, I described above, you can do it using the following steps:

1. Create a python file and name it display_userin_**something**.py in the **drivers** directory. Change **something** to something meaningful.
2. Add the following functions and code it using the python modules provided by the hardware vendor:
  * def init()
  * def destroy()
  * def read()
  * def list_buttons()
  * Note - I will add more functions in the future as I enhance the functionality of the console.
3. Edit the properties file in the **/etc** directory that you created earlier and add the driver information by adding the following line:
  * userInputDriver=**something**
4. Restart Chariot. If everything works, you should see be able to interact with the menu.

## Extending the Menu from your Application

You may have already found out that the console provides limited amount of functionality. While I have plans for enhancing the console significantly, it is primarily meant for applications to create their own user interface. To learn how to create your own applications on the Chariot framework, read https://bitbucket.org/iot4all/chariot/src/master/howto/application.md?fileviewer=file-view-default . 
