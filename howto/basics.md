# Basic Operations

## Before you start
To start with Chariot, at a minimum. you need a Raspberry Pi with the Raspbian image that is connected to a network either wirelessly or wired in. There are many useful resources that will help you with that. For example - https://learn.adafruit.com/adafruits-raspberry-pi-lesson-3-network-setup/setting-up-wifi-with-occidentalis . I also suggest setting up **ssh** so that you can connected to the Rpi remotely and transfer files. You can easily setup ssh using the raspi-config utility. You will also need **git** if you want to automatically get updates or want to contribute - *sudo apt-get install git* . You will need Python version 3. It typically comes pre-installed with the Raspbian image.

## Download Chariot

There are 2 ways you can do this.

1.Download the repository using a web browser - https://bitbucket.org/iot4all/chariot/downloads . Unzip into the *home/pi* directory and rename the top-level directory to **chariot**.
2. *cd /home/pi; git clone https://bitbucket.org/iot4all/chariot.git*

## One Time Setup

> *cd chariot*
> *chmod+x bin/`*`.sh*

Coming soon - how to setup Chariot as a system service.

## Running Chariot

Chariot needs a basic configuration file. I suggest adding it to the **etc** directory. Call it mychariot.properties or any other name. There are a few examples in the **etc** directory. I suggest copying the **sample.properties** file and making changes as needed. Once you are done with editing the file, run Chariot from the command line using the following command


> *cd chariot*
> *./run.sh*

This will print out the usage. At a minimum, you need to add the **-m** and the **-p** options but I also suggest adding the **-i** option. For example:

> *./run.sh -i wlan0 -m myfirstchariot -p etc/myproperty.properties*

Note that if you are connected on the Ethernet, replace **wlan0** with **eth0** above

This should startup the chariot application. At this point it does nothing but burn CPU cycles and power. To make it even remotely useful, you can startup a second Rpi with Chariot. If you don't have a second RPi, you can run the second instance of the Chariot by using the above steps but note the following:

1. The **-m** option should be different from the first one or else bad thing will happen.
2. You need to add a **-o** option to specify a different port from the first instance. The first instance uses the default port of 50000, the second instance must use something different.
3. You may want to consider providing a separate properties file for each of the instances

Once the second instance starts successfully, you will see log messages from each of the instance showing that they discovered each other.

Now, we are ready for more fun. Read the other howtos to find out what you can do - https://bitbucket.org/iot4all/chariot/src/master/howto/
:-)
