import RPi.GPIO as GPIO
import atexit
import time
import tempfile
import base64
import os

# run : sudo pip3 install schedule
import schedule

import picom
import pimessaging
import piutil
import piargs
import piconsole
import pisensor
import pireceiver
import pidiscovery
import pitopic
import piapp
import piremote
import pilogger
import pitts
import picam
import pioutput
import picaps

def exit_handler():
    global logger
    
    pidiscovery.destroy()
    picom.destroy()
    piconsole.destroy()
    piremote.destroy()
    pitts.destroy()
    picam.destroy()
    piapp.destroy()
    pisensor.destroy()
    pioutput.destroy()
    GPIO.cleanup()
    logger.info("Ended");
    
def main_loop():
    while True:
        event = pireceiver.receive()
        if event is not None:
            type = event[0]
            map = event[1]
            #print('Received event ' + type + ', ' + str(map))
            if type == 'multicast' and map['c'] in ['HELLO', 'HI', 'BYE']:
                pidiscovery.process(map)
            elif map['c'] == 'REQUEST' and map['f'] in pidiscovery.others:
                if map['s'] == 'MEASURE':
                    measurements = {}
                    pisensor.measure(measurements)
                    piapp.measure(measurements)

                    for key in measurements:
                        measurements[key] = str(measurements[key])
                    
                    picom.p2p_send(pimessaging.format_p2p_response(map['f'],
                                                                   map['i'],
                                                                   measurements),
                                   pidiscovery.others[map['f']]['h'],
                                   int(pidiscovery.others[map['f']]['p']))
                elif map['s'] == 'SURVEY':
                    path = None
                    if 'cam' in picaps.capabilities:
                        file = tempfile.mkstemp(suffix='.jpg')
                        os.close(file[0])
                        path = file[1]
                        picam.capture_image(path)

                        encoded = ''
                        if path:
                            with open(path, "rb") as f:
                                e = base64.b64encode(f.read())
                                encoded = e.decode('utf-8')
                            os.remove(path)

                        picom.p2p_send(pimessaging.format_p2p_response(map['f'],
                                                                       map['i'],
                                                                       {}, encoded),
                                       pidiscovery.others[map['f']]['h'],
                                       int(pidiscovery.others[map['f']]['p']))    
                elif map['s'] == 'OUTPUT':
                    params = piutil.to_object(map['p'])
                    pioutput.output(params)
                    piapp.output(params)
                    picom.p2p_send(pimessaging.format_p2p_response(map['f'],
                                                                   map['i'],
                                                                   {}),
                                pidiscovery.others[map['f']]['h'],
                                int(pidiscovery.others[map['f']]['p']))
                else:
                    # dont know about the subcommand, let the apps handle it
                    piapp.handle_event(map)
                   
            elif map['c'] == 'RESPONSE':
                topic = 'temp_response_' + map['i']
                if topic in pitopic.topics:
                    # obviously someone is interested in a response because they sent a request.
                    # The only reason the if check is there is because the response may have arrived
                    # after a timeout
                    pitopic.send_message(topic, map)
            else:
                # dont know about the event, let the apps handle it 
                piapp.handle_event(event)
                
        piconsole.process_user_input()

        piremote.process_user_input()
        
        # run any scheduled job that we have to
        schedule.run_pending()

        time.sleep(0.25) # sleep for 250


# main program
pilogger.system_logger_instance()
from pilogger import system_logger as logger

GPIO.setmode(GPIO.BCM)
piargs.parse_args()
pioutput.init()
pisensor.init()
picom.init()
pidiscovery.init()
piconsole.init()
piremote.init()
pitts.init()
picam.init()
pidiscovery.send_my_info('HELLO')
atexit.register(exit_handler)

piapp.init()
logger.info("Started")
main_loop()
