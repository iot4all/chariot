#!/bin/sh

path='.'
if [ -n "$CHARIOTPATH" ]; then
    path="$CHARIOTPATH"
    fi

cd $path    
export PYTHONPATH=bin:core:drivers:apps
python3 bin/pibot.py $*
