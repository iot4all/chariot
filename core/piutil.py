from ast import literal_eval

def load_properties(filename):
    props = {}
    for line in open(filename):
        line = line.strip()
        if line and not line.startswith("#"):
            tokens = line.split("=")
            if len(tokens) >= 2 and tokens[1].strip():
                props[tokens[0].strip()] = tokens[1].strip()                
    return props

def instantiate_class_by_name(mod, cl):
    module = __import__(mod, fromlist=[''])
    class_ = getattr(module, cl)
    return class_()

def to_object(str):
    return literal_eval(str)    
