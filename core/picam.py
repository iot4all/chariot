import piargs
import picaps

cam = False
cam_driver = None

def init():
    global cam_driver
    global cam
    if 'camDriver' in piargs.props:
        name = 'driver_cam_' + piargs.props['camDriver']
        cam_driver = __import__(name, fromlist=[''])
        cam_driver.init()
        cam = True
        picaps.add_capabilities(['cam'])

def destroy():
    global cam, cam_driver
    if not cam:
        return
    cam_driver.destroy()

def capture_image(filename):
    if not cam:
        return False
    cam_driver.capture_image(filename)
    return True
