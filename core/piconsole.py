import piargs
import pimenu
import picom
import pidiscovery
import picaps

MODE_TEXT = 0
MODE_MENU = 1
MODE_GRAPHICS = 2

display_driver = None
input_driver = None

mode = MODE_TEXT

cur_row = 0
cur_col = 0

current_path = ''
items = None
highlighted = 0
handler = None
current_txt = ''

def init():
    init_display()
    init_userin()

    if input_driver:
        display_menu()
    elif display_driver:
        draw_text('Chariot', 0, 0)
        draw_text('Me : ' + piargs.name, 1, 0)
        draw_text('IP : ' + picom.get_ip_address(), 2, 0)
        draw_text('Neighbors : ' + str(len(pidiscovery.others)), 3, 0)
        # TODO subscribe to the discovery topic and add the neighbor counts

def destroy():
    global display_driver, input_driver
    
    if display_driver:
        display_driver.clear((0,0,0))
    if input_driver:
        input_driver.destroy()

def init_display():
    global display_driver

    if 'displayDriver' in piargs.props:
        name = 'driver_display_' + piargs.props['displayDriver']
        display_driver = __import__(name, fromlist=[''])
        display_driver.init()

    if display_driver:
        picaps.add_capabilities(['display'])

def init_userin():
    global input_driver

    if 'userInputDriver' in piargs.props:
        name = 'driver_userin_' + piargs.props['userInputDriver']
        input_driver = __import__(name, fromlist=[''])
        input_driver.init()
        pimenu.init()
        picaps.add_capabilities(['input'])

def process_user_input():
    global highlighted, items, mode, handler, current_txt

    if input_driver:
        command = input_driver.read()
        if command:
            # print(command)
            if mode == MODE_MENU:            
                if command == 'NEXT':
                    draw_text(current_txt, highlighted+2, 0, False)
                    highlighted = highlighted+1
                    if highlighted == len(items) and not len(current_path):
                        highlighted = 0
                    elif highlighted > len(items):
                        highlighted = 0

                    if highlighted >= len(items):
                        current_txt = str(highlighted+1) + '. Back'
                    else:
                        current_txt = str(highlighted+1) + '. ' + items[highlighted][1]                       
                    draw_text(current_txt, highlighted+2, 0, True)
                elif command == 'ENTER':
                    if highlighted >= len(items):
                         index = current_path.rfind('/')
                         display_menu(current_path[0:index])
                    elif isinstance(items[highlighted][2], list):
                        path = current_path + '/' + items[highlighted][0]
                        display_menu(path)
                    else:
                        #reached a leaf, invoke action handler
                        path = current_path + '/' + items[highlighted][0]
                        handler = items[highlighted][2]
                        handler.action(path, command)
            elif handler:
                # if the handler has switched the mode to text mode, let it handle the commands
                handler.command(command)
                
def clear():
    global display_driver
    if not display_driver:
        return
    display_driver.clear()

def display_text(text):
    global cur_row, cur_col, display_driver, mode
    
    if not display_driver:
        return

    if mode == MODE_MENU:
        clear()
        cur_row = 0
        cur_col = 0
        mode = MODE_TEXT

    lines = text.split('\n')
    for line in lines:
        draw_text(line, cur_row, cur_col)
        cur_row = cur_row + 1
        cur_col = 0

def draw_text(text, row, col, reverse=False):
    global display_driver
    
    if display_driver:
        display_driver.draw_text(text, row, col, reverse)

def display_menu(path=''):
    global display_driver, items, mode, highlighted, current_path, current_txt
    
    if not display_driver:
        return

    if mode != MODE_MENU:
        clear()
        mode = MODE_MENU
        handler = None
        current_path = ''

    display_driver.clear()
    highlighted = 0
    
    current_path = path
    index = 1
    items = pimenu.get_choices(path)
    reverse = True

    draw_text('Please select from the following:', 0, 0)
    for item in items:
        txt = str(index) + '. ' + item[1]
        draw_text(txt, index+1, 0, reverse)
        if reverse:
            current_txt = txt
        index = index + 1
        reverse = False

    if path:
        draw_text(str(index) + '. Back', index+1, 0, False)

    if 'NEXT' in list_buttons():
        index = index + 3
        draw_text('Press the NEXT key to navigate', index, 0, False)

    if 'ENTER' in list_buttons():
        index = index + 1
        draw_text('Press the ENTER key to select', index, False)

    if 'HOME' in list_buttons():
        index = index + 1
        draw_text('Press the HOME key to go back to the main menu', index, False)

def reset_text():
    global cur_row, cur_col
    if not display_driver:
        return
    clear()
    cur_row = cur_col = 0

def list_buttons():
    return input_driver.list_buttons()

def resume_menu():
    display_menu(current_path)
