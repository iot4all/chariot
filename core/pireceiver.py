import select
import traceback
import sys

import picom
import pimessaging
import piargs
import piconsole

def receive():
    from pilogger import system_logger as logger
    event = None
    try:
        r, w, e = select.select([picom.multicast_s, picom.p2p_s], [], [], 0)
        type = None
        message = None
        if picom.multicast_s in r:
            message = picom.multicast_receive()
            type = 'multicast'
        elif picom.p2p_s in r:
            message = picom.p2p_receive()
            type = 'p2p'

        if type:
            parsed = parse_msg(message)
            if parsed:
                event = (type, parsed)
    except:        
        logger.warning('Error receving events. It may be because of a bad message received from another chariot', traceback.print_exc())
    return event

def parse_msg(message):
    map = pimessaging.parse_message(message)

    if map['f'] == piargs.name:
        # Its my own message
        return None

    if 't' in map and map['t'] != piargs.name:
        # The message is not for me
        return None
    
    return map
