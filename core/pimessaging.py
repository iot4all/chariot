#sudo pip3 install pycrypto

import picom
import piargs
import picaps

crl_id = 0

def parse_message(message):
    m = {}
    body = ''
    header = True
    lines = message.split('\r\n')
    for line in lines:
        if not len(line):
            header = False
        if header:
            words = line.split(':')
            key = words[0].strip()
            value = ''
            for i in range(1,len(words)):
                if len(value): value = value + ':'
                value = value + words[i]
                value = value.strip()
                # print(key + "=" + value)
                m[key] = value
        else:
            if len(body): body += '\r\n'
            body += line

    m['body'] = body
    # TODO throw exception if mandatory parameters are not present 
    return m

def format_my_info(command, location, map={}):
    msg = 'c: ' + command + '\r\nf: ' + piargs.name + '\r\nh: ' + picom.get_ip_address()
    msg = msg + '\r\nl: ' + piargs.location + '\r\np: ' + str(piargs.port)
    if len(picaps.capabilities):
        msg = msg + '\r\nx: ' +  str(picaps.capabilities)
        
    for key,value in map.items():
        msg += '\r\n' + key + ":" + value
    # print(msg)
    return msg

def format_p2p_request(to, map={}, body=''):
    global crl_id
    
    id = crl_id
    msg = 'c: REQUEST\r\nf: ' + piargs.name + '\r\nt: ' + to + '\r\ni: ' + str(id) + '\r\nl: ' + piargs.location
    crl_id += 1
    for key,value in map.items():
        msg += '\r\n' + key + ":" + value
    if len(body):
        msg += '\r\n\r\n' + body
    return (id, msg)

def format_p2p_response(to, id, map={}, body=''):
    msg = 'c: RESPONSE\r\nf: ' + piargs.name + '\r\nt: ' + to + '\r\ni: ' + str(id) + '\r\nl: ' + piargs.location
    for key,value in map.items():
        msg += '\r\n' + key + ":" + str(value)
    if len(body):
        msg += '\r\n\r\n' + body
    return msg
