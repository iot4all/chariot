import piargs
import pimenu
import pimenu_handlers

apps = {}

def init():
    global apps
    if 'apps' in piargs.props:
        tokens = piargs.props['apps'].split(',')
        for token in tokens:
            name = 'app_' + token
            app = __import__(name, fromlist=[''])
            app.init()
            apps[name] = app

def destroy():
    global apps
    for key in apps:
        apps[key].destroy()

def handle_event(event):
    global apps
    for key in apps:
        if apps[key].handle_event(event):
            return

def measure(map):
    global apps
    for key in apps:
        apps[key].measure(map)

def output(outputlist):
    global apps
    for key in apps:
        apps[key].output(outputlist)
