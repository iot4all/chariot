import piargs
import piutil
import picaps
import mappings

analogin = False
analog_drivers = []

def init():
    global analog_drivers
    global analogin
    
    if 'analogInputDrivers' in piargs.props:
        index = 0
        tokens = piargs.props['analogInputDrivers'].split(',')
        for token in tokens:
            parts = token.split(':')
            driver = piutil.instantiate_class_by_name('driver_analog_in_' + parts[0], parts[1])
            param = 'a' + str(index)
            if (len(parts) > 2):
                param = parts[2]
            driver.param = 'measure_' + param
            driver.init()
            analog_drivers.append(driver)
            index = index+1
        analogin = True

    if analogin:
        picaps.add_capabilities(['sensor'])

def destroy():
    pass

def measure(map):
    if analogin:
        for driver in analog_drivers:
            seq = 0
            list = driver.read()
            for e in list:
                device = driver.param + '_' + str(seq)

                var_name = device
                if hasattr(mappings, 'variable_' + device):
                    var_name = getattr(mappings, 'variable_' + device)()
                        
                unit = ''
                if hasattr(mappings, 'unit_' + device):
                    unit = getattr(mappings, 'unit_' + device)()

                value = e
                if hasattr(mappings, 'map_' + device):
                    value = getattr(mappings, 'map_' + device)(e)

                description = None
                if hasattr(mappings, 'description_' + device):
                    description = getattr(mappings, 'description_' + device)()

                unit_description = None
                if hasattr(mappings, 'unit_description_' + device):
                    unit_description = getattr(mappings, 'unit_description_' + device)()
                
                result = (var_name, value, unit, description, unit_description, 'AIN')
                
                map[device] = result
                seq = seq + 1
