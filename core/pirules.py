# measurements are provided as follows:
#     key = value, unit, type, location

def run(rules, facts, history):
    # we need to clone because the remove_rule/update_rule/append_rule can alter the rules list
    cloned_rules = list(rules)
    context = {}
    context['facts'] = facts
    context['history'] = history
    context['rules'] = rules
    context['output'] = {}
    for i, rule in enumerate(cloned_rules):
        context['index'] = i
        if rule['when'](context):
            rule['then'](context)
            if 'exit' in context:
                context.pop('exit')
                break
    #print(str(context['output']))
    return context['output']

def exit(context):
    context['exit'] = True
    
def update_action(context, key, value, type, location='', fact=False, unit=''):
    if location not in context['output']:
        context['output'][location] = {} 
    context['output'][location][key] = (value, unit, type)
    if fact:
        update_fact(context, key, value, type, unit, location)

def remove_action(context, key, location=''):
    if location in context['output'] and key in context['output'][location]:
        context['output'][location].pop(key)

def update_fact(context, key, value, type, unit, location=''):
    if location not in context['facts']:
        context['facts'][location] = {}
    context['facts'][location][key] = (value, unit, type)

def remove_fact(context, key, location=''):
    if location in context['facts'] and key in context['facts'][location]:
        context['facts'][location].pop(key)

def valueof(context, key, index, default, location = ''):
    ret = default
    if location in context['facts'] and key in context['facts'][location]:
        ret = context['facts'][location][key][index]
    return ret

def last_valueof(context, key, type, index, default, last, location = ''):
    ret = default
    history = context['history']
    if len(history):
        last = history[last]
        if location in last[type] and key in last[type][location]:
            ret = last[type][location][key][index]
    return ret

def history(context):
    return context['history']

def last_value(context, key, default, location = ''):
    return last_valueof(context, key, 1, 0, default, -1, location)

def last_action(context, key, default, location = ''):
    return last_valueof(context, key, 2, 0, default, -1, location)

def value(context, key, default, location = ''):
    return valueof(context, key, 0, default, location) 

def type(context, key, location = ''):
   return valueof(context, key, 1, '', location)

def unit(context, key, location = ''):
   return valueof(context, key, 2, '', location)

def insert_rule(context, after_rule_name, rule):
    for i,rule in enumerate(context[2]):
        if rule['name'] == after_rule_name:
            context['rules'].insert(i+1, rule)
            return

def remove_rule(context, rule_name):
    for rule in context['rules']:
        if rule['name'] == rule_name:
            context['rules'].remove(rule)
            return
        
def replace_rule(context, rule_name, rule):
    for i, r in enumerate(context['rules']):
        if r['name'] == rule_name:
            context['rules'][i] = rule
            return
        
def fire_rules(context, rules):
    fire(rules, context['facts'], context['history'])
