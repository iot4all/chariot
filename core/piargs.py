import argparse
import configparser

import picaps

name = None
interface = None
location = None
hostname = None
props = {}
port = 50001
key = ''

def parse_args():
    global name, interface, location, hostname, props, port, key

    parser = argparse.ArgumentParser(description='A script that is a part of the pi network')
    parser.add_argument('-m', metavar='myName', type=str, nargs=1, help='a unique name the identifies your pie', required=True)
    parser.add_argument('-i', metavar='interface', type=str, nargs='+',
        help='the name of the network interface that is used to talk to the rest of the world', 
        required=False)
    parser.add_argument('-n', metavar='hostname', type=str, nargs='+', 
        help='the hostname used to talk to the rest of the world)', 
        required=False)
    parser.add_argument('-l', metavar='location', type=str, nargs='+', default=['home'], 
        help='the name of the location where the pi is located (example: bedroom, bathroom, etc.)', 
        required=False)
    parser.add_argument('-o', metavar='port', type=str, nargs='+', 
        help='point to point communication port',
        required=False)
    parser.add_argument('-p', metavar='propertyFile', type=str, nargs='+', 
        help='configuration file', 
        required=False)
    parser.add_argument('-x', metavar='capabilities', type=str, nargs='+', 
        help='comma-separated capabiltiies', 
        required=False)
    parser.add_argument('-k', metavar='encryptionKey', type=str, nargs='+', default='chariot890123456',
        help='encryption key', 
        required=False)

    args = parser.parse_args()
    name = args.m[0]
    key = args.k
    
    if args.i:
        interface=args.i[0]

    if args.l:
        location=args.l[0]

    if args.n:
        hostname = args.n[0]

    if args.o:
        port = int(args.o[0])

    if args.p:
        config = configparser.ConfigParser()
        config.read(args.p[0])
        props = config['DEFAULT']
        #print(props)
        
    if args.x:
        caps = args.x[0].split(',')
        picaps.add_capabilities(caps)
