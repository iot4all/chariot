import piargs
import pitopic
import picaps

remote_driver = None

def init():
    global remote_driver
    if 'remoteDriver' in piargs.props:
        name = 'driver_remote_' + piargs.props['remoteDriver']
        remote_driver = __import__(name, fromlist=[''])
        remote_driver.init()
        pitopic.create_topic('remote')
        picaps.add_capabilities(['remote'])

def destroy():
    pitopic.remove_topic('remote')

def process_user_input():
    from pilogger import system_logger as logger
    global remote_driver
    if not remote_driver:
        return
    key = remote_driver.get_key()
    if not key:
        return

    logger.debug('Key: ' + key + ' pressed on the remote')
    pitopic.send_message('remote', key)
