import socket
import struct
import piargs
from Crypto.Cipher import AES

PACKET_SEND_SIZE = 500
PACKET_RECEIVE_SIZE = PACKET_SEND_SIZE + 10

multicast_s = None
p2p_s = None
fcntl = None

def init() :
    global multicast_s, p2p_s, p2p_port, fcntl

    multicast_addr="239.192.1.100"
    if 'multicastAddr' in piargs.props:
        multicast_addr = piargs.props['multicastAddr']

    multicast_port=50000
    if 'multicastPort' in piargs.props:
        port = int(piargs.props['multicastPort'])

    if piargs.interface is not None:
        name = 'fcntl'
        fcntl = __import__(name, fromlist=[''])
        # print(fcntl)

    multicast_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    multicast_s.setblocking(0)

    multicast_s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    multicast_s.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_TTL, 20)
    multicast_s.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_LOOP, 1)

    multicast_s.bind(('', multicast_port))
    intf = get_ip_address()
    multicast_s.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(intf))
    multicast_s.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_addr) + socket.inet_aton(intf))
    # print ("Connected to the pinet on " + intf + " to the multicast network " + multicast_addr + " and port " + str(multicast_port))

    p2p_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    p2p_s.setblocking(0)
    p2p_s.bind(('', piargs.port))
    # print ("Connected to the pinet on " + intf + " to the p2p network port " + str(piargs.port))
    
def destroy():
    global multicast_s
    global p2p_s
    multicast_s.close()
    p2p_s.close()

def multicast_send(message, addr='239.192.1.100', port=50000):
    global multicast_s
    send(multicast_s, message, addr, port)

def p2p_send(message, addr, port):
    global p2p_s
    send(p2p_s, message, addr, port)

def send(s, message, addr, port):
    remaining = len(message)
    offset = 0
    while remaining > 0:
        size = min(remaining, PACKET_SEND_SIZE)
        remaining = remaining - size
        header = 'E\r\n'
        if remaining > 0: header = 'C\r\n'
        chunk =  header + message[offset:offset+size]        
        #print('Sending(' + addr + ':' + str(port) + ')[' + str(chunk) + ']')
        encryption = AES.new(piargs.key, AES.MODE_CFB, 'Chariot890123456')
        packet = encryption.encrypt(chunk)
        s.sendto(packet, (addr, port))
        offset = offset + size

def multicast_receive():
    global multicast_s
    return receive(multicast_s)

def p2p_receive():
    global p2p_s
    return receive(p2p_s)

def receive(s):
    buffer = ''
    while True:
        packet, sender = s.recvfrom(PACKET_RECEIVE_SIZE)        
        encryption = AES.new(piargs.key, AES.MODE_CFB, 'Chariot890123456')
        chunk = encryption.decrypt(packet).decode('utf-8')
        #print('Received(' + str(sender) + ')[' + str(chunk) + ']')
        index = chunk.index('\r\n')
        header = chunk[0:index]
        message = chunk[index+2:]
        buffer = buffer + message
        if header == 'E': break

    # print(buffer)
    return buffer

def get_ip_address():
    global multicast_s
    global fcntl

    if piargs.interface is None:
        hostname = piargs.hostname
        if piargs.hostname is None:
            hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
    else:
        a = struct.pack('256s', bytearray(piargs.interface[:15], 'utf-8'))
        ip = socket.inet_ntoa(fcntl.ioctl(multicast_s.fileno(),
            0x8915, # SIOCGIFADDR
            a)[20:24])
    # print('The IP address is (' + ip + ')')
    return ip
