import piargs
import piutil

menu_tree = []
menu_ready = False

def init():
    global menu_tree
    global menu_ready
    menu_tree = [
        ('device', 'Device',
            [('config', 'Configuration', init_handler('pimenu_handlers:PiMenuHandler')),
                ('measures', 'Measurements', init_handler('pimenu_handlers:PiMenuHandler'))]),
        ('network', 'Network', init_handler('pimenu_handlers:PiMenuHandler')),
        ('application', 'Applications', init_handler('pimenu_handlers:PiMenuHandler'))
    ]
    menu_ready = True

def init_handler(name):
    tokens = name.split(':')
    handler = piutil.instantiate_class_by_name(tokens[0], tokens[1])
    return handler

def get_choices(path, validate = True):
    context = menu_tree
    if len(path):
        if path[0] == '/': path = path[1:]
        tokens = path.split('/')
        splits = len(tokens)
        for token in tokens:
            splits = splits - 1
            for element in context:
                if token == element[0]:
                    context = element[2]
                    if validate and not isinstance(context, list) and splits > 0:
                        # we have not reached the end of the path but it looks like a leaf
                        return None
                    break     
    return context

def add_to_menu(path, submenu):
    global menu_ready
    if not menu_ready:
        return
    parent,child = split_path(path)
    choices = get_choices(parent, False)
    if choices:
        for n,i in enumerate(choices):
            if i[0] == child:
                m = submenu
                if isinstance(choices[n][2], list):
                     m = choices[n][2] + submenu
                choices[n] = (choices[n][0], choices[n][1], m)
                break
            
def replace_menu(path, submenu):
    global menu_ready
    if not menu_ready:
        return
    parent,child = split_path(path)
    choices = get_choices(parent)
    if choices:
        for n,i in enumerate(choices):
             if i[0] == child:
                choices[n] = submenu
                break

def remove_menu(path):
    global menu_ready
    if not menu_ready:
        return    
    parent,child = split_path(path)
    choices = get_choices(parent)
    if choices:
        for choice in choices:
            if choice[0] == child:
                choices.remove(choice)
                break

def split_path(path):
    if path[0] == '/': path = path[1:]
    parent = ''
    child = path

    index = path.rfind('/')
    if index > 0:
        parent = path[0,index]
        child = path[index+1:]
    return (parent,child)
