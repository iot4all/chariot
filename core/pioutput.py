import piargs
import piutil
import picaps
import mappings

digitalout = False
digital_drivers = []

def init():
    global digital_drivers
    global digitalout
    
    if 'digitalOutputDrivers' in piargs.props:
        index = 0
        tokens = piargs.props['digitalOutputDrivers'].split(',')
        for token in tokens:
            parts = token.split(':')
            driver = piutil.instantiate_class_by_name('driver_digital_out_' + parts[0], parts[1])
            param = 'd' + str(index)
            if (len(parts) > 2):
                param = parts[2]
            driver.param = 'output_' + param
            driver.init()
            digital_drivers.append(driver)
            index = index+1
        digitalout = True

    if digitalout:
        picaps.add_capabilities(['output'])

def destroy():
    pass

def output(outputlist):
    if digitalout:
        for device in outputlist:
            value = outputlist[device]
            if value[2] == 'DOUT':
                for driver in digital_drivers:
                    dev_name = device
                    if hasattr(mappings, 'device_output_' + device):
                        dev_name = getattr(mappings, 'device_output_' + device)()
                    if dev_name.startswith(driver.param):
                        driver.output(dev_name, value)

