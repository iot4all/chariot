topics = {}

def create_topic(name):
    topics[name] = []

def add_listener(name, listener):
    topics[name].append(listener)

def send_message(name, message):
    for l in topics[name]:
       l.new_message(name, message)

    if name.startswith('temp_'):
        topics.pop(name)

def remove_topic(name):
    if name in topics: topics.pop(name)
