import logging

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

system_logger = None

def system_logger_instance():
    global system_logger
    if not system_logger:
        system_logger = create_logger()

def create_logger(name='chariot', filename='chariot.log', level=logging.ERROR):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(FORMAT)

    if filename:
        fh = logging.FileHandler('log/' + filename)
        fh.setLevel(level)
        fh.setFormatter(formatter)
        
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger
