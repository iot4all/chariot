import piargs
import picaps

tts_driver = None

def init():
    global tts_driver
    if 'ttsDriver' in piargs.props:
        name = 'driver_tts_' + piargs.props['ttsDriver']
        tts_driver = __import__(name, fromlist=[''])
        tts_driver.init()
        picaps.add_capabilities(['speech'])

def destroy():
    pass

def speak(text):
    global tts_driver
    if not tts_driver:
        return
    tts_driver.speak(text)
