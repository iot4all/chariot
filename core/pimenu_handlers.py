import schedule

import piargs
import piconsole
import picom
import pidiscovery
import pimenu
import pitopic
import pimessaging
import pisensor
import piutil

topic = None
wait = False
timeout_job = None

def help_text(next=True, enter=True):
    text = ''
    if next and 'NEXT' in piconsole.list_buttons():
        text = text + '\n\nPress the NEXT key to go back'
    if enter and 'ENTER' in piconsole.list_buttons():
        text = text + '\nPress the ENTER key to refresh'
    return text

def print_settings():
    piconsole.reset_text()
    piconsole.display_text('Configuration:\n')
    text = '\nMe : ' + piargs.name + '\nIP : ' + picom.get_ip_address() + '\nLocation : ' + piargs.location
    text = text + help_text()
    piconsole.display_text(text)       

def print_neighbor(name):
    piconsole.reset_text()
    other = pidiscovery.others[name]
    piconsole.display_text('Network:\n')
    text = '\nName : ' + name + '\nIP : ' + other['h'] + '\nLocation : ' + other['l']
    text = text + help_text()    
    piconsole.display_text(text)    

def print_measures(message):
    piconsole.reset_text()
    piconsole.display_text('Measurements from ' + message['f'] + '\n')
    count = 0
    for key in message:
        if key.startswith('measure_'):
            t = piutil.to_object(message[key]) #parse the dictionary object from string
            if t[0].startswith('unused'):
                    continue
            name = t[0]
            if len(t) >= 4 and t[3]:
                name = t[3]
                
            if name.startswith('measure_'):
                name = t[0][len('measure_'):]

            text = name + ':\n' + 'Value: ' + str(t[1]) + ' ' + t[2]
            if count:
                piconsole.display_text('\n')
            piconsole.display_text(text)
            count = count + 1
    text = help_text()
    piconsole.display_text(text)
    
class PiResponseHandler:
    def new_message(self, topic, message):
        global wait, timeout_job
        print_measures(message)
        schedule.cancel_job(timeout_job)
        timeout_job = None
        wait = False
        
def timeout():
    global wait, topic, timeout_job
    from pilogger import system_logger as logger
    logger.error('Timeout receiving measurements from a remote Chariot')
    pitopic.remove_topic(topic)
    wait = False
    timeout_job = None
    return schedule.CancelJob

class PiMenuHandler:
    current_path = None

    def __init__(self):
        pitopic.add_listener('discovery', self)

    def new_message(self, topic, message):
        if pidiscovery.others:
            l = []
            for key in pidiscovery.others:
                l.append((key, key + ': ' + pidiscovery.others[key]['l'],
                          [('config', 'Configuration', self),
                           ('measures', 'Measurements', self)]))
            pimenu.replace_menu('network', ['network', 'Network', l])
        else:
            pimenu.replace_menu('network', ['network', 'Network', self])
    
    def action(self, path, command):
        if path[0] == '/': path = path[1:]
        self.current_path = path
        self.display()

    def command(self, command):
        global wait
        if wait:
            return
        if command == 'NEXT':
            piconsole.resume_menu()
        elif command == 'ENTER':
            self.display()

    def display(self):
        if self.current_path == 'device/config':
            print_settings()
        elif self.current_path == 'device/measures':
            map = {}
            pisensor.measure(map)
            map['f'] = piargs.name
            print_measures(map)
        elif self.current_path.startswith('network'):
            tokens = self.current_path.split('/')
            if len(tokens) == 1:
                piconsole.reset_text()
                piconsole.display_text('There are no devices' + help_text(True, False))
            else:
                name = tokens[1]
                type = tokens[2]
                if type == 'config':
                    if name in pidiscovery.others:
                        print_neighbor(name)
                    else:
                        piconsole.reset_text()                    
                        piconsole.display_text('The device is gone' + help_text(True, False))
                else:
                    global topic, wait, timeout_job
                    if name in pidiscovery.others:
                        piconsole.reset_text()                    
                        piconsole.display_text('Sending request ...' + help_text(True, False))
                        id, msg = pimessaging.format_p2p_request(name, {'s': 'MEASURE'})
                        picom.p2p_send(msg, pidiscovery.others[name]['h'], int(pidiscovery.others[name]['p']))
                        topic = 'temp_response_' + str(id)
                        pitopic.create_topic(topic)
                        handler = PiResponseHandler()
                        pitopic.add_listener(topic, handler)
                        timeout_job = schedule.every(30).seconds.do(timeout)
                        wait = True
                    else:
                        piconsole.reset_text()                    
                        piconsole.display_text('The device is gone' + help_text(True, False))
        elif self.current_path.startswith('application'):
            piconsole.reset_text()                    
            piconsole.display_text('There are no application' + help_text(True, False))
