import picom
import pimessaging
import pitopic
import piutil

others = {}

def send_my_info(command):
    map = {}
    picom.multicast_send(pimessaging.format_my_info(command, map))

def init():
    from pilogger import system_logger as logger
    pitopic.create_topic('discovery')

def destroy():
    send_my_info('BYE')

def process(map):
    from pilogger import system_logger as logger
    if map['c'] == 'HELLO':
        if map['f'] not in others:
            msg = map['f'] + " signed in with IP address: " + map['h'] + ', location: ' + map['l']
            if 'x' in map:
                msg += ", with capabilities - " + map['x']
            logger.info(msg)
        send_my_info('HI')
        if 'x' in map:
            map['x'] = piutil.to_object(map['x'])
        others[map['f']] = map
        if len(others) == 1:
            logger.info('I have company')
    elif map['c'] == 'HI':
        if map['f'] not in others:
            msg = map['f'] + " discovered with IP address: " + map['h'] + ', location: ' + map['l']
            if 'x' in map:
                msg += ", with capabilities - " + map['x']
            logger.info(msg)
            
        if 'x' in map:
            map['x'] = piutil.to_object(map['x'])

        others[map['f']] = map
        if len(others) == 1:
            logger.info('I have company')
    elif map['c'] == 'BYE':
        logger.info(map['f'] + " signed out")
        others.pop(map['f'], None)
        if not others:
            logger.info('it is lonely out here')

    pitopic.send_message('discovery', 'CHANGE')

def find_by_capabilities(capabilities):
    return find('x', capabilities)

def find_by_location(locations):
    return find('l', locations)

def find(param, values):
    # print('find ' + param + ' with values ' + str(values))
    global others
    list = []
    dest = set(values)
    for other in others.values():
        # print('other : ' + str(other))
        if param in other:
            l = other[param]
            if isinstance(l, str):
                l = [l]
            src = set(l)
            # print('src: ' + str(src))
            result = src.intersection(dest)
            if len(result):
                list.append(other['f'])
    return list
