
# plus a lot of setup including:
# (1) kernel module config
# (2) create remote key configuration file using irrecord (irrecord -l - to list)
# running : lircd --device /dev/lirc0
# Creating /home/pi/.lircrc
# sudo apt-get install python{,3}-lirc

import lirc

def init():
    sock = lirc.init('chariot', blocking = False)
    lirc.load_config_file('etc/remote.conf')

def get_key():
    key = None
    codeir = lirc.nextcode()
    
    if len(codeir):
        key = codeir[0]
    return key
