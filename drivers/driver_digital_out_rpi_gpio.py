import RPi.GPIO as GPIO

PORT_1 = 5
PORT_2 = 6

ports = [
    PORT_1,
    PORT_2
]

devices = {}

class RPIGPIO:
    param = None

    def init(self):
        global ports, devices
        i = 0
        for port in ports:
           GPIO.setup(port, GPIO.OUT)
           devices[self.param + '_' + str(i)] = ports[i]
           i = i+1

    def output(self, name, value):
        global devices
        #print('output to ' + str(devices[name]) + ', value = ' + str(value[0]))
        GPIO.output(devices[name], value[0])
