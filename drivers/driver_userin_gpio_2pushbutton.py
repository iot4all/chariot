import RPi.GPIO as GPIO

PB1_PORT = 20
PB2_PORT = 16

buttons = [
    ['NEXT', PB1_PORT, False],
    ['ENTER', PB2_PORT, False]
    ]

def init():
    for button in buttons:
        GPIO.setup(button[1], GPIO.IN)

def destroy():
    pass

def read_button(button):
    b = None
    input = GPIO.input(button[1])
    if input and not button[2]:
        b = button[0]
    button[2] = input
    return b

def read():
    for button in buttons:
        b = read_button(button)
        if b is not None:
            return b
    return None

def list_buttons():
    l = []
    for button in buttons:
        l.append(button[0])
    return l
