from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import Adafruit_ILI9341 as TFT
import Adafruit_GPIO as GPIO
import Adafruit_GPIO.SPI as SPI

import piargs

SCREEN_WIDTH = 240
SCREEN_HEIGHT = 320

# Raspberry Pi GPIO configuration.
DC = 18
RST = 23
SPI_PORT = 0
SPI_DEVICE = 0

BACKGROUND = (255,0,0)
FOREGROUND = (255,255,255)

font = None
disp = None
draw = None
row_height = None
col_width = None
max_lines = 0
max_columns = 0

def init():
    global font
    global disp
    global col_width
    global row_height
    global max_lines
    global max_columns
    global draw

    disp = TFT.ILI9341(DC, rst=RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=64000000))

    font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeSans.ttf', 16)
    #font = ImageFont.load_default()
    disp.begin()
    disp.clear((255,0,0))
    draw = ImageDraw.Draw(disp.buffer)
    col_width,row_height = draw.textsize('A', font=font)
    max_lines = SCREEN_HEIGHT / row_height
    max_columns = SCREEN_WIDTH / col_width

def clear(color=None):
    global disp
    if not color: color = BACKGROUND
    disp.clear(color)

def draw_text(text, row, col, reverse=False):
    global font
    global disp
    global draw
    global row_height
    global col_height

    fg = FOREGROUND
    bg = BACKGROUND
    if reverse:
        bg = FOREGROUND
        fg = BACKGROUND
    
    width,height = draw.textsize(text, font=font)
    textimage = Image.new('RGBA', (width, height), bg)
    textdraw = ImageDraw.Draw(textimage)
    textdraw.text((0,0), text, font=font, fill = fg)

    # TODO handle screen orientation - potrait/landscape
    rotated = textimage.rotate(0, expand=1)
    disp.buffer.paste(rotated, (col * int(col_width), row * int(row_height)), rotated)
    disp.display()
