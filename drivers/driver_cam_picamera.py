# sudo apt-get install python-picamera
# also need to enable camera using raspi-config
# works only with the RPi camera module, not with USB web cams

import picamera

hflip = False
vflip = False
camera = None

def init():
    global camera, hflip, vflip
    camera = picamera.PiCamera()
    camera.hflip = hflip
    camera.vflip = vflip

def destroy():
    pass

def capture_image(filename):
    global camera
    camera.capture(filename)
