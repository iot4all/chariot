import Adafruit_ADS1x15

GAIN = 1

class ADS1x15:
    adc = None
    param = None

    def init(self):
         self.adc = Adafruit_ADS1x15.ADS1015()

    def read(self):
        values = [0]*4
        for i in range(4):
            values[i] = self.adc.read_adc(i, gain=GAIN)
        return values
