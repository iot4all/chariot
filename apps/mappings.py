import piargs

variable_measure_a0_1 = lambda: 'light'

description_measure_a0_1 = lambda: 'The light for ' + piargs.location

unit_measure_a0_1 = lambda: 'lm'

unit_description_measure_a0_1 = lambda : 'Lumen'

map_measure_a0_1 = lambda value: value

description_measure_a0_2 = lambda: 'The temperature for ' + piargs.location

variable_measure_a0_2 = lambda: 'temperature'

unit_measure_a0_2 = lambda: 'C'

unit_description_measure_a0_2 = lambda: 'Centigrade'

map_measure_a0_2 = lambda value: value/10

variable_measure_a0_0 = lambda: 'unused_a0_0'

variable_measure_a0_3 = lambda: 'unused_a0_3'

device_output_light_switch = lambda: 'output_d0_0' 
