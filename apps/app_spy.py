#pip3 install schedule
import schedule
import time
import base64
import os
import datetime
from pathlib import Path

import picom
import pimessaging
import pidiscovery
import pitopic
import pilogger
import piconsole
import pimenu
import piutil
import piargs

LOCATION = 'artifacts/apps/spy'
MAX_FILES = 5

logger = None
response_handler = None
wait = False
request_ids = []
timeout_job = None
collect_job = None
in_app_display = False
scan_time = None

class MenuHandler:
    def action(self, path, command):
        global in_app_display
        in_app_display = True
        piconsole.reset_text()
        piconsole.display_text('Please wait...')

    def command(self, command):
        global in_app_display
        if command == 'NEXT':
            piconsole.resume_menu()
        elif command == ' ENTER':
            piconsole.reset_text()
            piconsole.display_text('Please wait...')

class SurveyResponseHandler:
    def new_message(self, topic, message):
        global wait, request_ids, timeout_job, current, logger
        request_ids.remove(topic)
        process_survey(message)
        
        if not len(request_ids):
            # all responses received
            schedule.cancel_job(timeout_job)
            timeout_job = None
            wait = False

def process_survey(message):
    if 'body' in message:
        location = LOCATION + '/' + message['l']
        if not Path(location).exists():
            os.mkdir(location)
        
        path = location + '/' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.jpg'
        with open(path, "wb") as f:
            b = base64.b64decode(message['body'])
            f.write(b)
            
        files = sorted(os.listdir(location))
        print(str(files))
        if len(files) > MAX_FILES:
            files_to_del = len(files) - MAX_FILES
            for i in range(0, files_to_del):
                os.unlink(location + '/' + files[i])
                

def timeout():
    global wait, request_ids, logger, timeout_job
    logger.error('Timeout receiving survey from all remote Chariots - ' + str(request_ids))
    request_ids = []
    wait = False
    timeout_job = None
    return schedule.CancelJob


def collect():
    global response_handler, wait, logger, timeout_job, request_ids, scan_time
    if wait:
        # we have not finished with the previous collection - impossible
        return

    scan_time = time.time()
    bots = pidiscovery.find_by_capabilities(['cam'])
    
    if not len(bots):
        logger.info('No remote chariots found with cam capability, skipping')
        return
    
    for bot in bots:
        other = pidiscovery.others[bot]
        id, msg = pimessaging.format_p2p_request(bot, {'s': 'SURVEY'})
        picom.p2p_send(msg, pidiscovery.others[bot]['h'], int(pidiscovery.others[bot]['p']))
        topic = 'temp_response_' + str(id)
        pitopic.create_topic(topic)
        pitopic.add_listener(topic, response_handler)
        request_ids.append(topic)
    timeout_job = schedule.every(2).minutes.do(timeout)
    wait = True

def destroy():
    global collect_job
    schedule.cancel_job(collect_job)

def init():
    global logger, response_handler, collect_job
    logger = pilogger.create_logger('spy')
    collect_job = schedule.every(1).minutes.do(collect)
    response_handler = SurveyResponseHandler()
    handler = MenuHandler()
    pimenu.add_to_menu('application', [('spy', 'Spy', handler)])
    
    if not Path(LOCATION).exists():
            os.mkdir(LOCATION)
            
    logger.info('Started application Spy')
    
def measure(map):
    pass

def handle_event(event):
    pass

def output(outputlist):
    pass
