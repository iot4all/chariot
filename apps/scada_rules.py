from pirules import *

rules = [
    {
        'name': 'low_light_sensor',
        'when': lambda context: value(context, 'light', 0, 'kitchen') < 300,
        'then': lambda context: update_fact(context, 'low_light_sensor', True, 'DIN', '', 'kitchen')
    },    
    {
        'name': 'turn_on_light_on_low_light',
        'when': lambda context: value(context, 'low_light_sensor', False, 'kitchen')
        and not last_value(context, 'low_light_sensor', False, 'kitchen'),
        'then': lambda context: update_action(context, 'light_switch', True, 'DOUT', 'kitchen')
    },
    {
        'name': 'turn_off_light_on_low_light',
        'when': lambda context: value(context, 'light', 0, 'kitchen') >= 310,
        'then': lambda context: update_action(context, 'light_switch', False, 'DOUT', 'kitchen')
    },
    {
        'name': 'raise_alarm_on_high_temperature',
        'when': lambda context: value(context, 'temperature', 0, 'kitchen') > 40,
        'then': lambda context: update_fact(context, 'temperature_alarm', 'MAJOR', 'ALARM', '', 'kitchen')
    }
]
