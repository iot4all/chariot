import schedule
import time
import pisensor
import pimenu
import piconsole
import pilogger
import pitopic

timestamp = None
count = 0
logger = None

def help_text(next=True, enter=True):
    text = ''
    if next and 'NEXT' in piconsole.list_buttons():
        text = text + '\n\nPress the NEXT key to go back'
    if enter and 'ENTER' in piconsole.list_buttons():
        text = text + '\nPress the ENTER key to refresh'
    return text

def print_app_info():
    piconsole.reset_text()
    piconsole.display_text('Data Logger Information:\n')
    piconsole.display_text('Last Scanned:\n' + str(timestamp) + '\nMeasurements:\n' + str(count) + help_text())

class MenuHandler:
    def action(self, path, command):
        print_app_info()
    def command(self, command):
        if command == 'NEXT':
            piconsole.resume_menu()
        elif command == 'ENTER':
            print_app_info()
          
def log():
    global timestamp
    global count
    global logger
    
    count = 0
    map = {}
    timestamp = time.ctime()
    ticks = time.time()
    pisensor.measure(map)
    for key in map:
        value = map[key]
        if value[0].startswith('unused'):
            continue
        count = count+1
        logger.info(str(ticks) + ',' + value[0] + ',' + str(value[1]) + ',' + value[2])

def init():
    global logger
    logger = pilogger.create_logger('dataLogger')
    handler = MenuHandler()
    pimenu.add_to_menu('application', [('dataLogger', 'Data Logger', handler)])

    schedule.every(1).minutes.do(log)
    logger.info('Started application dataLogger')
 
def destroy():
    schedule.cancel_job(log)

def handle_event(event):
    pass

def measure(map):
    pass

def output(outputlist):
    pass
