import pilogger
import pitopic
import pitts
import pisensor

logger = None

class RemoteHandler:
      def new_message(self, topic, message):
          if message == '1':
              take_measurements()

def take_measurements():
    text = ''
    map = {}
    pisensor.measure(map)
    for key in map:
        value = map[key]
        if value[0].startswith('unused'):
            continue
        name = value[0]
        if value[3]:
            name = value[3]
        unit = value[2]
        if value[4]:
            unit = value[4]  
        pitts.speak(name + ' is ' + str(value[1]) + ' ' + unit + ', ')

def destroy():
    pass

def handle_event(event):
    pass

def measure(map):
    pass

def output(outputlist):
      pass

def init():
    global logger
    logger = pilogger.create_logger('measurementsReader')

    if 'remote' in pitopic.topics:
        pitopic.add_listener('remote', RemoteHandler())

    logger.info('Started measurements reader')
    pitts.speak('Hello this is you Chariot Measurements Reader. I am at your command')

