#pip3 install schedule
import schedule
import time

import picom
import pimessaging
import pidiscovery
import pitopic
import pilogger
import piconsole
import pimenu
import piutil
import pirules
import piargs
import pioutput
import piapp

import scada_rules

HISTORY_SIZE = 5

logger = None
measure_response_handler = None
output_respone_handler = None
wait = False
measure_request_ids = []
output_request_ids = []
measure_timeout_job = None
output_timeout_job = None
collect_job = None
in_app_display = False
current = {}
history = []
display_buffer = ''
scan_time = None
actions = None

class MenuHandler:
    def action(self, path, command):
        global in_app_display, display_buffer
        in_app_display = True
        piconsole.reset_text()
        if len(display_buffer):
            piconsole.display_text(display_buffer)
        else:
            piconsole.display_text('Please wait...')
    def command(self, command):
        global in_app_display, display_buffer
        if command == 'NEXT':
            piconsole.resume_menu()
            in_app_display = False
        elif command == ' ENTER':
            piconsole.reset_text()
            if len(display_buffer):
                piconsole.display_text(display_buffer)
            else:
                piconsole.display_text('Please wait...')

class OutputResponseHandler:
    def new_message(self, topic, message):
        global output_request_ids, output_timeout_job, wait
        output_request_ids.remove(topic)
        if not len(output_request_ids):
            schedule.cancel_job(output_timeout_job)
            output_timeout_job = None
            wait = False
    
class MeasurementsResponseHandler:
    def new_message(self, topic, message):
        global wait,measure_request_ids, measure_timeout_job, current, history, display_buffer, actions, logger, output_request_ids, output_timeout_job
        measure_request_ids.remove(topic)
        process_measures(message)
        
        if not len(measure_request_ids):
            # all responses received
            schedule.cancel_job(measure_timeout_job)
            measure_timeout_job = None

            if len(scada_rules.rules):
                actions = pirules.run(scada_rules.rules, current, history)
                print('Action: ' + str(actions))
                if len(actions):
                    for location in actions:
                        if location == piargs.location:
                            # This is for me
                            pioutput.output(actions[location])
                            piapp.output(actions[location])
                            pass
                        else:
                            locations = [location]
                            bots = pidiscovery.find_by_location(locations)
                            if len(bots):
                                bot = bots[0]
                                outputs = actions[location]
                                id, msg = pimessaging.format_p2p_request(bot,
                                                                         {'s': 'OUTPUT',
                                                                          'p': str(outputs)})
                                picom.p2p_send(msg, pidiscovery.others[bot]['h'], int(pidiscovery.others[bot]['p']))
                                topic = 'temp_response_' + str(id)
                                pitopic.create_topic(topic)
                                pitopic.add_listener(topic, output_response_handler)
                                output_request_ids.append(topic)
                            else:
                                logger.warn('No location was found matching the requested action - ' + location)
                                
            if len(output_request_ids):
                output_timeout_job = schedule.every(10).seconds.do(output_timeout)
            else:
                wait = False

            format_measures()
            if in_app_display and len(display_buffer):
                piconsole.reset_text()
                piconsole.display_text(display_buffer)

def format_measures():
    global current, display_buffer
    for location in current:
        text = '\n\nLocation: ' + location + ':\n'
        display_buffer = display_buffer + text
        values = current[location]
        for name in values:
            value = values[name]
            text = '\n    ' + name + ': ' + str(value[0]) + ' ' + value[1]
            display_buffer = display_buffer + text
    
def process_measures(message):
    global current

    location = ''
    if 'l' in message:
        location = message['l']

    current[location] = {}
         
    for key in message:
        if key.startswith('measure_'):
            t = piutil.to_object(message[key]) #parse the dictionary object from string
            if t[0].startswith('unused'):
                    continue
                
            name = t[0]
            if name.startswith('measure_'):
                name = t[0][len('measure_'):]

            # value, unit, type, location
            current[location][name] = (t[1], t[2], t[5])

def measure_timeout():
    global wait, measure_request_ids, logger,measure_timeout_job
    logger.error('Timeout receiving measurements from all remote Chariots - ' + str(measure_request_ids))
    measure_request_ids = []
    wait = False
    measure_timeout_job = None
    return schedule.CancelJob

def output_timeout():
    global wait, output_request_ids, logger, output_timeout_job
    logger.error('Timeout receiving output response from all remote Chariots - ' + str(output_request_ids))
    output_request_ids = []
    wait = False
    output_timeout_job = None
    return schedule.CancelJob

def collect():
    global measure_response_handler, wait, measure_request_ids, logger, measure_timeout_job, current, history, display_buffer, scan_time, actions
    if wait:
        # we have not finished with the previous collection - impossible
        return

    if len(current):
        history.append((scan_time, current, actions))
        if len(history) >= HISTORY_SIZE:
            # delete the oldest
            del(history[0])

    measure_request_ids = []
    output_response_ids = []
    scan_time = time.time()
    current = {}
    
    bots = pidiscovery.find_by_capabilities(['sensor'])
    
    if not len(bots):
        logger.info('No remote chariots found with sensor capability, skipping')
        return
    
    for bot in bots:
        other = pidiscovery.others[bot]
        id, msg = pimessaging.format_p2p_request(bot, {'s': 'MEASURE'})
        picom.p2p_send(msg, pidiscovery.others[bot]['h'], int(pidiscovery.others[bot]['p']))
        topic = 'temp_response_' + str(id)
        pitopic.create_topic(topic)
        pitopic.add_listener(topic, measure_response_handler)
        measure_request_ids.append(topic)
    measure_timeout_job = schedule.every(10).seconds.do(measure_timeout)
    wait = True
    display_buffer = 'Scan: ' + time.ctime()

def destroy():
    global collect_job
    schedule.cancel_job(collect_job)

def init():
    global logger, measure_response_handler, collect_job, output_response_handler
    logger = pilogger.create_logger('scada')
    collect_job = schedule.every(30).seconds.do(collect)
    measure_response_handler = MeasurementsResponseHandler()
    output_response_handler = OutputResponseHandler()
    handler = MenuHandler()
    pimenu.add_to_menu('application', [('scada', 'SCADA', handler)])
    logger.info('Started application SCADA')
    
def measure(map):
    pass

def handle_event(event):
    pass

def output(outputlist):
    pass
