#Chariot - IOT on Wheels

##Background

IOT is becoming more and more prevalent in everyday life. In a few more years, pretty much all home appliances will be on the Internet with users having access and control from anywhere. I am ready to join the IOT bandwagon and develop a few cool IOT gadgets.

I started this project to get back into doing some low-level programming that allows me to interact with electronic circuits. Few years back, I started with an Arduino Uno. While that was a great experience, I did eventually outgrow it and needed something that allows me to do more. I had a Raspberry Pi even then but I was basically using it as a low-cost Linux system. In the last few years, there has been a lot of new projects in the hardware programming areas of the RPi. Lots of drivers that earlier worked on Arduino only have ow been ported to RPi. Also, the recent introduction of the RPi Zero enables us to develop very powerful applications using the power of Linux while retaining the benefit of the Zero's low footprint. The RPi 3, on the other end of the spectrum, has pretty much one needs to develop very serious IOT applications - processing power, on board wireless and bluetooth, camera-support and more. A RPi developer can easily leverage all the powerful tools that are already available on Linux including compilers, text to speech capabilities, among others, and combine with many inexpensive hardware peripherals available on the RPi to build powerful IOT applications.

##About Chariot

The Chariot project, pronounced as Char-I-O-T, is an attempt to build a framework using which IOT developers can quickly develop and run applications, In a way, it is like the dreaded application servers Java provides, hopefully, much lighter. In this model, IOT developers can plugin new device interfaces, add small applications that utilize the framework and connect to a network of Chariot devices. The framework provides basic capabilities like logging, scheduling, discovery, measurements among other things. It also allows applications to get plugged into a custom TFT console, a remote control, remote shell and much more.

This project is still in its infancy. Please read the documents in the howto directory to learn more about things you can do - https://bitbucket.org/iot4all/chariot/src/master/howto/

Here is the recommended list in case you are short on time

1. Basic Setup - https://bitbucket.org/iot4all/chariot/src/master/howto/basics.md?fileviewer=file-view-default
2. Setting up a Console - https://bitbucket.org/iot4all/chariot/src/master/howto/console.md?fileviewer=file-view-default
3. Creating your own Application - https://bitbucket.org/iot4all/chariot/src/master/howto/application.md?fileviewer=file-view-default
